package com.itheima.mq;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/12 17:14
 */
@Component
public class EmailConsumer {

    @Resource
    private JavaMailSenderImpl javaMailSender;

    @Value("${spring.mail.username}")
    private String sender;

    @RabbitListener(queues = "email_queue")
    public  void receiver(String email) {
            try {
                SimpleMailMessage message = new SimpleMailMessage();
                message.setSubject("KlausMax.top官方邮件");
                message.setText("你好，请保存好验证码：9988，邮件发送测试成功！");
                message.setTo(email);
                message.setFrom(sender);
                javaMailSender.send(message);
            } catch (MailException e) {
                e.printStackTrace();
            }
            System.out.println("邮件发送成功");
    }
}
