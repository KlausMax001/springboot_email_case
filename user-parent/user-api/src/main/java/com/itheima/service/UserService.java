package com.itheima.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.pojo.User;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/10 22:35
 */
public interface UserService extends IService<User> {


     boolean register(User user);
}
