package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.User;
import org.springframework.stereotype.Repository;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/10 23:04
 */

@Repository
public interface UserMapper extends BaseMapper<User> {
}
