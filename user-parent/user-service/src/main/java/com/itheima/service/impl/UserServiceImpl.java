package com.itheima.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.mapper.UserMapper;
import com.itheima.pojo.User;
import com.itheima.service.UserService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.amqp.core.AmqpTemplate;

import javax.annotation.Resource;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/10 23:39
 */
//Dubbo暴露接口
@DubboService
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    //注入MQ对象准备发送邮件信息
    @Resource
    private AmqpTemplate rabbitTemplate;


    @Override
    public boolean register(User user) {
        //1.保存用户信息
        this.save(user);

        //2.发送mq
        rabbitTemplate.convertAndSend("emailExchange","email",user.getEmail());

        return true;
    }
}
