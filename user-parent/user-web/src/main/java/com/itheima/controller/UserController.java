package com.itheima.controller;

import com.itheima.pojo.User;
import com.itheima.service.UserService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/11 16:26
 */

@RestController
@RequestMapping("/user")
public class UserController{

    @DubboReference(check = false)
    private UserService userService;


    @GetMapping("/findAll")
    public List<User> findAll(){
        return userService.list();
    }

    @PostMapping("/register")
    public boolean register(@RequestBody User user){
        return userService.register(user);
    }
}
